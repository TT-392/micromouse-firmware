# micromouse-firmware

This is our firmware for the 2024 RSA MicroMouse competition.

## Dependencies

The exact dependencies to install differ per OS.

### Arch Linux

	sudo pacman -S cmake ninja arm-none-eabi-gcc arm-none-eabi-newlib base-devel

### Debian/Ubuntu

	sudo apt update
	sudo apt install cmake ninja-build gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential

## Cloning the project

	git clone --recursive -j8 https://gitlab.com/nloomans/micromouse-firmware

Note that this may take a while due to the large amount of submodules within
the tinyusb submodule of the pico-sdk submodule. On my machine this takes about
2 minutes to complete.

## Creating the build dir

	cmake -B build -G Ninja

Using Ninja is not required, but it is recommended as it provides a significant
speed improvement over make.

## Compiling the project

	cmake --build build

## Flashing the code

### Manual

Hold the BOOTSEL button while connecting to the device. A removable media
should appear. Copy the `build/micromouse_firmware.uf2` file to the removable
media.

### Picotool

Install the Picotool package. This can often be installed using your package
manager:

[![Picotool packaging
status](https://repology.org/badge/vertical-allrepos/picotool.svg)](https://repology.org/project/picotool/versions)

Otherwise, you may follow the instructions
[here](https://github.com/raspberrypi/picotool) to manually compile it.

Then, you may flash the code and reboot the device into application mode by
running:

	picotool load build/micromouse_firmware.uf2
	picotool reboot

If the device is already in application mode, run:

	picotool load -f build/micromouse_firmware.uf2

To return to BOOTSEL mode, run:

	picotool reboot -uf

## License

This project is released under the GPLv3 license. See `LICENSE` for details.
