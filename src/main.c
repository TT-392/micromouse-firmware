#include "pico/binary_info.h"
#include "pico/stdlib.h"
#include <stdio.h>

int main()
{
    bi_decl(bi_program_description("Micromouse Firmware"));
    bi_decl(bi_program_version_string("v0.0.1"));

    stdio_init_all();
    while (1) {
        printf("Hello, World!\n");
        sleep_ms(1000);
    }
}
